import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  @Input() setHero : any ='';
  colorCode = '#ffbaba';

  constructor() {
  }

  ngOnInit(): void {

  }

  setColor(power : any) : string{
    // console.log(power);
    if(power >=80){
      return '#CB4335';
    }else if(power >=60){
      return '#E74C3C';
    }else if(power >=40){
      return '#EC7063';
    }else if(power >=20){
      return '#F1948A'
    }else if(power >=0){
      return '#F5B7B1'
    }else{
      return '#ffffff'
    }

    console.log(this.colorCode);
  }

}
